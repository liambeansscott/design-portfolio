import { useEffect, useState } from 'react'
type IntersectionObserverOptions = {
    threshold: number,
    rootMargin: string,
}
const useOnScreen = ( options: IntersectionObserverOptions ) => {
    const [ref, setRef]: any = useState( null );
    const [visible, setVisible] = useState( false )
    useEffect( () => {
        const observer = new IntersectionObserver( ( [entry] ) => {
            setVisible( entry.isIntersecting )
        }, options )
        if ( ref ) {
            observer.observe( ref )
        }
        return () => { if ( ref ) { observer.unobserve( ref ) } }
    }, [ref, options] )

    return { ref: setRef, visible: visible }

}


export default useOnScreen