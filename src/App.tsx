import React, { useEffect } from 'react';
import LandingPage from './pages/LandingPage/LandingPage';
import Gallery from './pages/Gallery/Gallery'
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import axios from 'axios'

function App() {
  let cats: any[] = []
  const catImages = () => {
    axios.defaults.headers.common['x-api-key'] = "9910c2fd-0743-493f-9e4b-49c571d333fe"
    for ( let i = 0; i < 20; i++ ) {
      axios.get( 'https://api.thecatapi.com/v1/images/search' )
        .then( response =>
          cats.push( response.data[0].url )
        )

    }
    return cats;
  }
  // useEffect( () => {
  //   console.log( 'effectcalled' )
  //   catImages();
  // }, [] )
  return (
    <BrowserRouter>
      <div className="App">
        <Switch>
          <Route path="/gallery">
            <Gallery images={cats} />
          </Route>
          <Route path="/">
            <LandingPage />
          </Route>

        </Switch>

      </div>
    </BrowserRouter>

  );
}

export default App;
