import React from 'react';
import Form from '../components/form/form';

export default [
  {
    id: 0,
    background: 'enquiryImage',
    active: false,
    title: 'enquiries',
    description: 'make an enquiry about a commision',
    content: '',
    reverseSide: {
      id: 0,
      background: '',
      active: false,
      title: '',
      content: <Form />,
    },
  },
  {
    id: 1,
    background: 'drip',
    active: false,
    title: 'drip',
    description: 'work for a start up skate brand in Nicaragua',

    content: '',
  },
  {
    id: 2,
    background: 'creps',
    active: false,
    title: 'creps',
    description: 'saving beaters from landfill',
    content: '',
  },
  {
    id: 3,
    background: 'gallery',
    active: false,
    title: 'gallery',
    description: 'collection of more works',
    content: '',
    link: 'gallery',
  },
];
