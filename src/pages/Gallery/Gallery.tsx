import React, { useEffect, useState } from 'react'
import classes from './Gallery.module.css';
import axios from 'axios'


const Gallery = ( props: any ) => {
    const [images, setImages]: any[] = useState( [] )
    const [columns, setColumns]: any = useState( {
        0: [], 1: [], 2: []
    } )

    useEffect( () => {
        let imagesTemp: any[] = []
        axios.defaults.headers.common['x-api-key'] = "9910c2fd-0743-493f-9e4b-49c571d333fe"
        axios.get( 'https://api.thecatapi.com/v1/images/search', { params: { limit: 20, size: "full" } } )
            .then( response => {
                imagesTemp = response.data.map( ( data: any ) => {
                    return <img className={classes.galleryImage} src={data.url} />
                } )
                setImages( imagesTemp )

            } )


    }, [] )


    useEffect( () => {
        console.log( 'catschanged triggering effect ', images )
        let columnsNew: any = {
            0: [], 1: [], 2: []
        }
        images.map( ( image: any, index: number ) => {
            columnsNew[index % 3].push( image )
            console.log( 'adding image: ', image, 'to column', index % 3 )
        } )
        setColumns( columnsNew )
    }, [images] )

    useEffect( () => { console.log( 'columns changed triggering effect ', columns ) }, [columns] )

    return (
        <>
            <div className={classes.title}>gallery</div>
            <div className={classes.galleryGrid}>
                <div className="galleryColumns">
                    {columns[0]}
                </div>
                <div className="galleryColumns">
                    {columns[1]}
                </div>
                <div className="galleryColumns">
                    {columns[2]}
                </div>
            </div>



        </>

    )
}

export default Gallery;