import React from 'react';
import Navigation from '../../components/navigation/navigation';
import PhotoCards from '../../components/photoCards/photoCards';

import data from '../../data/photocards';
import classes from './LandingPage.module.css';

const LandingPage = () => {
  return (
    <div className={classes.landingPage}>
      {/* REACT ROUTER ANIMATION */}
      <Navigation />
      <PhotoCards photoCardsData={data} />
    </div>
  );
};

export default LandingPage;
