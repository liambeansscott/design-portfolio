export default interface PhotoCardProps {
    id: number;
    background: string;
    title: string;
    active: boolean;
    content: React.ReactNode;
    handleActive: Function;
    handleCardFlip: Function;
    description: string;
    reverseSide?: PhotoCardProps;
    link?: string;
};