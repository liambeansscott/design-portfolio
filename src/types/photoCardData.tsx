export default interface photoCardData {
  id: number;
  background: string;
  active: boolean;
  title: string;
  description: string;
  content: React.ReactNode;
  reverseSide?: any;
  link?: string;
}
