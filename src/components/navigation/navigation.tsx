import React from 'react';
import classes from './navigation.module.css'
import { Link } from 'react-router-dom'

const navigation = () => {
    return (
        <div className={classes.navigation}>
            <div className={classes.title}>Liam &nbsp; &nbsp; Scott</div>
            <nav className={classes.navbar}>
                <li>enquiries</li>
                <li>drip</li>
                <li>creps</li>
                <Link to="/gallery">
                    <li>gallery</li>
                </Link>

            </nav>
        </div>
    )
}

export default navigation;