import React from 'react';
import classes from './popOver.module.css'
import { Link } from 'react-router-dom'
type PopOverProps = {
    content: string | React.ReactNode;
    handleCardFlip: Function | any;
    description: string;
    link?: string;
}

const popOver: React.FC<PopOverProps> = ( {
    content,
    handleCardFlip,
    description,
    link
} ) => {
    const exploreLink = <div className={classes.linkWrap}>
        {handleCardFlip ? <p className={classes.exploreLink} onClick={() => handleCardFlip()}>explore</p> :
            <p className={classes.exploreLink}>explore</p>
        }
    </div>
    return (
        <div className={classes.popOver}>
            {content}
            <div className={classes.descriptionWrap}><p className={classes.description}>{description}</p></div>
            {link ?
                <Link to={'/' + link}>
                    {exploreLink}
                </Link>
                :
                exploreLink
            }



            {/* {handleClick ? <p className={classes.exploreLink} onClick={() => handleClick()}>explore</p> : null} */}
        </div >
    )
}

export default popOver;