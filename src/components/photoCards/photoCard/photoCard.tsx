import React, { useEffect } from 'react';
import classes from './photoCard.module.css';
import PopOver from '../../popOver/popOver';
import useOnScreen from '../../../hooks/onScreen'
import PhotoCardProps from '../../../types/photoCardProps'

const PhotoCard: React.FC<PhotoCardProps> = ( {
  id,
  active,
  handleActive,
  background,
  title,
  content,
  handleCardFlip,
  reverseSide,
  description,
  link
} ) => {



  const { ref, visible } = useOnScreen( { threshold: 0.9, rootMargin: '0px' } );
  const annotationLetters: JSX.Element[] | null =
    title ?
      title.split( "" ).map( ( letter: string, index: number ) =>
        <div key={index} className={classes.annotationLetterContainer}> < p className={active ? classes.annotationLetterShow : classes.annotationLetterHide}> {letter}</p ></div>
      ) : null;

  const annotation: JSX.Element | null = annotationLetters ? < div className={classes.annotation} > {[...annotationLetters]}</div > : null

  useEffect( () => {
    if ( visible ) {
      handleActive( id )
    }
  }, [visible, handleActive, id] )

  return (
    <div
      ref={ref}
      className={classes.photoCard} onClick={() => handleActive( id )}
      style={{
        background: `url(${`/images/${background}.png`}) left center/316px 316px no-repeat `,
      }}>
      {active ? <div className={classes.mask} /> : null}

      <div className={classes.overlay}>
        {annotation}
        {active ?
          <PopOver link={link} description={description} handleCardFlip={reverseSide ? () => handleCardFlip( id ) : null} content={content} />
          : null}



      </div>


    </div >
  )

};

export default PhotoCard;
