import React, { useState, useCallback } from 'react';
import PhotoCard from './photoCard/photoCard';
import classes from './photoCards.module.css';
import photoCardData from '../../types/photoCardData';

type Props = {
  photoCardsData: photoCardData[];
};


const PhotoCards = ( props: Props ) => {
  const [activeCard, setActiveCard] = useState( 0 );
  const [cards, setCards] = useState( props.photoCardsData )

  const changeActiveCardHandler = useCallback( ( activeCardId: number ) => {
    setActiveCard( activeCardId );
  }, [] )



  const handleCardFlip = ( cardId: number ) => {
    if ( cardId === activeCard ) {
      const cardCopy: photoCardData = { ...cards[cardId] }
      const reverseSideCopy = { ...cardCopy.reverseSide }
      const newReverseSide = Object.keys( cardCopy )
        .filter( key => key !== 'reverseSide' ).reduce( ( obj: any, key ) => {
          return {
            ...obj,
            [key]: cardCopy[key as keyof photoCardData]
          }
        }, {}
        )
      const flippedPhotoCard: photoCardData = {
        ...reverseSideCopy,
        reverseSide: newReverseSide,
      }
      const cardsCopy = [...cards]
      cardsCopy[cardId] = flippedPhotoCard
      setCards( cardsCopy )
    }

  }
  const photoCards = cards.map( ( photoCard ) => (
    <PhotoCard
      id={photoCard.id}
      background={photoCard.background}
      title={photoCard.title}
      content={photoCard.content}
      key={photoCard.id}
      active={activeCard === photoCard.id}
      reverseSide={photoCard.reverseSide}
      handleActive={() => changeActiveCardHandler( photoCard.id )}
      handleCardFlip={() => handleCardFlip( photoCard.id )}
      description={photoCard.description}
      link={photoCard.link}
    />
  ) );


  return (
    <div className={classes.photoCards}>
      {photoCards}
    </div>
  );
};

export default PhotoCards;
