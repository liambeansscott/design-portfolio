import React from 'react'
import classes from './form.module.css';

const form = () => {
    const handleSubmit = ( event: any ) => {
        alert( 'enquiry sent' )
    };
    return (
        <form className={classes.form}>
            <textarea
                className={classes.inputEmail}
                id="email"
                name="email"
                placeholder="contact e-mail"
            ></textarea>
            <textarea
                className={classes.inputEnquiry}
                id="enquiry"
                name="enquiry"
                placeholder="enquiry"
            ></textarea>
            <button onClick={handleSubmit}>Submit</button>

        </form >

    )
}

export default form;